$('.btn-to-top').click(function() {
    $(window).scrollTop(0);
});

function toggleNav() {
    // Show the nav button and allow it to open the sidebar
    $('#sidebarButton').click(function() {
        // If the sidebar is showing...
        if ($('.sidebar').width() >= $(document).width()) {
            // ... then remove the sidebar
            $('.sidebar').width('0%');
            $('.sidebar').addClass('d-none');
            // ... and increase the size of the main area
            $('.main').width('100%');
            $('.main').removeClass('d-none');
        }
        // If the sidebar is hidden...
        else {
            // ... then add the sidebar
            $('.sidebar').width('100%');
            $('.sidebar').removeClass('d-none');
            // ... and remove the main area
            $('.main').width('0%');
            $('.main').addClass('d-none');
        }
    });

    // Show the nav button and allow it to open the sidebar
    $('.sidebar-box a').click(function() {
        // If the sidebar is showing...
        if ($('.sidebar').width() >= $(document).width()) {
            // ... then remove the sidebar
            $('.sidebar').width('0%');
            $('.sidebar').addClass('d-none');
            // ... and increase the size of the main area
            $('.main').width('100%');
            $('.main').removeClass('d-none');
        }
    });
}

function toggleSearch() {
    // If we're on a small screen, we want to take up the whole nav
    if ($(window).width() <= 768) {
        $('#search-button').click(function() {
            $('#search-button').addClass('d-none');
            $('.navbar-brand').addClass('d-none');
            $('#search-form').removeClass('d-none');
            $("#search-input").focus();
        });
        $('#search-close-button').click(function() {
            $('#search-button').removeClass('d-none');
            $('.navbar-brand').removeClass('d-none');
            $('#search-form').addClass('d-none');
        });
    } else {
        $('#search-button').click(function() {
            $('#search-button').addClass('d-none');
            $('#search-form').removeClass('d-none');
            $("#search-input").focus();
        });
        $('#search-close-button').click(function() {
            $('#search-button').removeClass('d-none');
            $('#search-form').addClass('d-none');
        });
    }
}

$(document).ready(function() {
    toggleNav();
    toggleSearch();
});
