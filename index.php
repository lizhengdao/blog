<?php
// SQL Server Information
$servername = getenv('DB_SERVER_NAME');
$username = getenv('USER_NAME');
$password = getenv('PASSWORD');
$dbname = getenv('DB_NAME');

// Trim the leading slashes & split the path on slashes
$path = ltrim($_SERVER['REQUEST_URI'], '/');
$elements = explode('/', $path);

// Show home if there are no paramters or paths
if(empty($elements[0])) {
    ShowHomepage($servername,$username,$password,$dbname);
}
// Else, identify the first item in the URL so we can identify which function to use
else switch(array_shift($elements)) {
    case 'article':
        ShowArticle($elements,$servername,$username,$password,$dbname);
        break;
    case 'category':
        ShowCategory($elements,$servername,$username,$password,$dbname);
        break;
    case 'editor':
        ShowEditor();
        break;
    case 'search':
        ShowSearch($elements,$servername,$username,$password,$dbname);
        break;
    case 'rss':
    case 'rss.xml':
        ShowRSS($servername,$username,$password,$dbname);
        break;
    default:
        header('HTTP/1.1 404 Not Found');
        Show404Error();
}

function ShowHomepage($servername,$username,$password,$dbname) {
    // Connection
    $con = new mysqli($servername, $username, $password, $dbname);
    if ($con->connect_error) {
        die("Connection failed: " . $con->connect_error);
    }

    // SQL Query
    $sql = "SELECT * FROM article ORDER BY id DESC";
    $result = mysqli_query($con,$sql);

    // Add Articles
    $content_col = '';
    $stack = array();
    while($row = mysqli_fetch_array($result)) {
        $year = date_format(date_create($row['created']),"Y");
        if (!in_array($year, $stack)) {
            $content_col .= '<h1 class="text-center">' . $year . '</h1>';
            array_push($stack, $year);
        }
        $content_col .= '<section class="card article py-4 mb-4"><h2 class="article-title"><a href='.$row['link'].' aria-label="Read '.$row['title'].' blog post">'.$row['title'].'</a></h2><div><time class="m-0 d-inline" datetime='.date_format(date_create($row['created']),"Y-m-d").'>'.date_format(date_create($row['created']),"Y-m-d").'</time><p class="author m-0 ml-2 d-inline">:: Christian Cleberg</p></div><div class="post-tags"><a href="https://blog.cleberg.io/category/'.$row['tag'].'" class="tag">#'.strtolower(str_replace(" ","-",$row['tag'])).'</a></div><p class="my-4">'.$row['description'].'</p><a class="article-link" href='.$row['link'].'>Read more &rarr;</a></section>';
    }

    // Close the Database Connection
    mysqli_close($con);

    // Fill the Template
    $page_link = "https://blog.cleberg.io";
    $page_description = 'Explore the thoughts of Christian Cleberg - cybersecurity auditor, data scientist, software developer, and avid learner.';
    $page_title = 'Blog | Christian Cleberg';
    $page_extras = '';
    $relative_path = '.';
    $main_class = 'home-main';
    include "template.php";
}

function ShowArticle($params,$servername,$username,$password,$dbname) {
    // URL Parameter
    $query = $params[0];

    // Connection
    $con = new mysqli($servername, $username, $password, $dbname);
    if ($con->connect_error) {
        die("Connection failed: " . $con->connect_error);
    }

    // SQL Query Insertion
    $sql = "SELECT t1.id,t1.title,t1.author,t1.description,t1.tag,t1.created,t1.modified,t1.modified,t2.id,t2.body FROM article t1 LEFT JOIN article_body t2 ON t1.id = t2.id WHERE t1.link LIKE '%".$query."%' ORDER BY t1.id DESC;";
    $result = mysqli_query($con,$sql);

    // Matching Articles
    $row = mysqli_fetch_array($result);

    // Markdown Content
    include 'Parsedown.php';
    // $file = file_get_contents('./cpp.md', FILE_USE_INCLUDE_PATH);
    $md = Parsedown::instance()->text($row['body']);
    $html = new DOMDocument();
    $html->loadHTML($md);
    $links = $html->getElementsByTagName('a');
    foreach($links as $link) {
        $link->setAttribute("rel", "noopener,noreferrer");
        $link->setAttribute("target", "_blank");
    }
    $secured_html = $html->saveHTML();

    // Echo Results
    $header = '<header><h1 class="blog-post-title">'.$row['title'].'</h1><div class="blog-post-metadata"><time class="d-inline" datetime="'.date_format(date_create($row['created']),"Y-m-d").'">'.date_format(date_create($row['created']),"Y-m-d").'</time><p class="mx-1 my-0 d-inline">::</p><a href="https://cleberg.io">'.$row['author'].'</a></div><div class="post-tags"><a href="https://blog.cleberg.io/category/'.$row['tag'].'">#'.$row['tag'].'</a></div></header>';
    $content_col = '<article class="blog-post">'.$header.'<section class="blog-post-body">' . $secured_html . '</section></article>';

    // Close the Database Connection
    mysqli_close($con);

    // Fill the Template
    $page_link = "https://blog.cleberg.io/article/" . $query;
    $page_description = $row['description'] . ' Read more at blog.cleberg.io!';
    $page_title = $row['title'] . ' | Blog by Christian Cleberg';
    $page_extras = '';
    $relative_path = '../..';
    $main_class = 'article-main';
    include "template.php";
}

function ShowCategory($params,$servername,$username,$password,$dbname) {
    // URL Parameter
    $query = $params[0];

    // Connection
    $con = new mysqli($servername, $username, $password, $dbname);
    if ($con->connect_error) {
        die("Connection failed: " . $con->connect_error);
    }

    // SQL Query Insertion
    $sql = "SELECT * FROM article WHERE (`tag` LIKE '%".$query."%') ORDER BY id DESC";
    $result = mysqli_query($con,$sql);

    // Open the Article Column
    $content_col = '<header><h1 class="text-center mb-4">Category Results</h1></header>';

    // Matching Articles
    while($row = mysqli_fetch_array($result)) {
        $content_col .= '<section class="card article py-4 mb-4"><h2 class="article-title"><a href='.$row['link'].' aria-label="Read '.$row['title'].' blog post">'.$row['title'].'</a></h2><div><time class="m-0 d-inline" datetime='.date_format(date_create($row['created']),"Y-m-d").'>'.date_format(date_create($row['created']),"Y-m-d").'</time><p class="author m-0 ml-2 d-inline">:: Christian Cleberg</p></div><div class="post-tags"><a href="https://blog.cleberg.io/category/'.$row['tag'].'" class="tag">#'.$row['tag'].'</a></div><p class="my-4">'.$row['description'].'</p><a class="article-link" href='.$row['link'].'>Read more &rarr;</a></section>';
    }

    // Close the Database Connection
    mysqli_close($con);

    // Fill the Template
    $page_link = "https://blog.cleberg.io/category/" . $query;
    $page_description = 'Browse all '.$row['tag'].' articles by Christian Cleberg.';
    $page_title = 'Category Search | Blog by Christian Cleberg';
    $page_extras = '';
    $relative_path = '../..';
    $main_class = 'home-main';
    include "template.php";
}

function ShowSearch($params,$servername,$username,$password,$dbname) {
    // URL Parameter
    $query = $params[0];

    // Connection
    $con = new mysqli($servername, $username, $password, $dbname);
    if ($con->connect_error) {
        die("Connection failed: " . $con->connect_error);
    }

    // SQL Query Insertion
    $sql = "SELECT * FROM article WHERE (`title` LIKE '%".$query."%') OR (`description` LIKE '%".$query."%') OR (`tag` LIKE '%".$query."%') ORDER BY id DESC";
    $result = mysqli_query($con,$sql);

    // Open the Article Column
    $content_col = '<header><h1 class="text-center mb-4">Search Results</h1></header>';

    // Matching Articles
    while($row = mysqli_fetch_array($result)) {
        $content_col .= '<section class="card article py-4 mb-4"><h2 class="article-title"><a href='.$row['link'].' aria-label="Read '.$row['title'].' blog post">'.$row['title'].'</a></h2><div><time class="m-0 d-inline" datetime='.date_format(date_create($row['created']),"Y-m-d").'>'.date_format(date_create($row['created']),"Y-m-d").'</time><p class="author m-0 ml-2 d-inline">:: Christian Cleberg</p></div><div class="post-tags"><a href="https://blog.cleberg.io/category/'.$row['tag'].'" class="tag">#'.$row['tag'].'</a></div><p class="my-4">'.$row['description'].'</p><a class="article-link" href='.$row['link'].'>Read more &rarr;</a></section>';
    }

    // Close the Database Connection
    mysqli_close($con);

    // Fill the Template
    $page_link = "https://blog.cleberg.io/search/" . $query;
    $page_description = 'Use the custom search page to find articles by keyword searches.';
    $page_title = 'Custom Search | Blog by Christian Cleberg';
    $page_extras = '';
    $relative_path = '../..';
    $main_class = 'home-main';
    include "template.php";
}

function ShowEditor() {
    // Show the Editor
    $content_col = '<header><h1 class="text-center mb-4">Create New Post</h1></header>';
    $content_col .= '<form action=submit_post.php method=POST><div class=form-row><div class="form-group col-md-6"><label for=inputUsername>Username</label><input class=form-control id=inputUsername name=inputUsername type=username></div><div class="form-group col-md-6"><label for=inputPassword>Password</label><input class=form-control id=inputPassword name=inputPassword type=password></div></div><div class=form-row><div class="form-group col-md-6"><label for=inputPageTitle>Page Title</label><input class=form-control id=inputPageTitle name=inputPageTitle></div><div class="form-group col-md-6"><label for=inputPageLink>Page Link</label><input class=form-control id=inputPageLink name=inputPageLink aria-describedby=pageLinkHelp><small id=pageLinkHelp class="form-text text-muted">Must begin with https://blog.cleberg.io/article/</small></div></div><div class=form-row><div class="form-group col-md-12"><label for=inputPageDescription>Page Description</label><input class=form-control id=inputPageDescription name=inputPageDescription type=text></div></div><div class=form-group><label for=inputCategory>Category</label><input class=form-control id=inputCategory name=inputCategory type=text></div><div class=form-group><label for=inputMarkdown>Post Editor (Markdown)</label><textarea class=form-control id=inputMarkdown name=inputMarkdown rows=5></textarea></div><button class="btn btn-danger" type=submit>Submit</button></form>';

    // Fill the Template
    $page_link = "https://blog.cleberg.io/editor/";
    $page_description = '';
    $page_title = 'Editor | Blog by Christian Cleberg';
    $page_extras = '<meta name="robots" content="noindex,nofollow">';
    $relative_path = '../..';
    $main_class = 'home-main';
    include "template.php";
}

function ShowRSS($servername, $username, $password, $dbname) {
    // Connection
    $con = new mysqli($servername, $username, $password, $dbname);
    if ($con->connect_error) {
        die('Connection failed: ' . $con->connect_error);
    }

    // SQL Query Insertion
    $sql = "SELECT t1.id,t1.title,t1.author,t1.description,t1.tag,t1.created,t1.modified,t1.modified,t2.id,t2.body FROM article t1 LEFT JOIN article_body t2 ON t1.id = t2.id WHERE t1.link LIKE '%".$query."%' ORDER BY t1.id DESC;";
    $result = mysqli_query($con,$sql);

    // Generate RSS XML
    header('Content-type: text/xml');
    $rss_contents = '<?xml version="1.0" encoding="UTF-8"?>
    <rss version="2.0"
        xmlns:content="http://purl.org/rss/1.0/modules/content/"
        xmlns:wfw="http://wellformedweb.org/CommentAPI/"
        xmlns:dc="http://purl.org/dc/elements/1.1/"
        xmlns:atom="http://www.w3.org/2005/Atom"
        xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
        xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
	>
        <channel>
            <atom:link href="https://blog.cleberg.io/rss.xml" rel="self" type="application/rss+xml" />
            <title>Blog | Christian Cleberg</title>
            <link>https://blog.cleberg.io</link>
            <description>This is the personal blog of Christian Cleberg. There are no specific topics to be covered in any of my blog posts but you can find information on auditing, security, software development, and other oddities in my life.</description>
            <copyright>Copyright 2017 - 2020, Christian Cleberg</copyright>
            <language>en-us</language>
            <docs>https://cyber.harvard.edu/rss/index.html</docs>
            <lastBuildDate>Sat, 31 Oct 2020 20:00:00 CST</lastBuildDate>
            <ttl>60</ttl>
            <image>
                <url>https://img.cleberg.io/share/profile.jpg</url>
                <title>Blog | Christian Cleberg</title>
                <link>https://blog.cleberg.io</link>
            </image>';

    include 'Parsedown.php';
    while($row = mysqli_fetch_array($result)){
        // Markdown content for the description tag
        $md = Parsedown::instance()->text($row['body']);
        $html = new DOMDocument();
        $html->loadHTML($md);
        $links = $html->getElementsByTagName('a');
        foreach($links as $link) {
            $link->setAttribute("rel", "noopener,noreferrer");
            $link->setAttribute("target", "_blank");
        }
        $secured_html = $html->saveHTML();

      $rss_contents .= '<item>
                            <title>'.str_replace(array('&', '<', '>'), array('&amp;', '&lt;', '&gt;'), $row['title']).'</title>
                            <author>'.str_replace(array('&', '<', '>'), array('&amp;', '&lt;', '&gt;'), $row['author']).'</author>
                            <dc:creator>'.str_replace(array('&', '<', '>'), array('&amp;', '&lt;', '&gt;'), $row['author']).'</dc:creator>
                            <link>'.str_replace(array('&', '<', '>'), array('&amp;', '&lt;', '&gt;'), $row['link']).'</link>
                            <pubDate>'.date_format(date_create($row['created']),"D, d M Y H:i:s T").'</pubDate>
                            <guid>'.str_replace(array('&', '<', '>'), array('&amp;', '&lt;', '&gt;'), $row['link']).'</guid>
                            <description><![CDATA['.$row['description'].']]></description>
                            <content:encoded><![CDATA['.$secured_html.']]></content:encoded>
                        </item>';
    }
    $rss_contents .= '</channel></rss>';

    echo $rss_contents;

    // You can save the contents generated above to an actual XML file, but it might cause errors when trying to show newly added articles
    // $rss_file = fopen('rss.xml', 'w');
    // fwrite($rss_file, $rss_contents);
    // fclose($rss_file);
    // header('Location: https://blog.cleberg.io/rss.xml');
    die();
}

function Show404Error() {
    header('Location: https://blog.cleberg.io/404.html');
    die();
}
?>
