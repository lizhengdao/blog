![](https://img.cleberg.io/blog/promo.png)

This is the personal blog of [Christian Cleberg](https://cleberg.io), Cybersecurity Auditor (CSX-A), PCEP, ALMI, ACS, & IBM Data Science Professional. This blog is not specifically tied to any single topic but some frequent topics are auditing, security, software development, and FOSS.  

If you have any questions, feel free to email me at [christian@cleberg.io](mailto:christian@cleberg.io).  

[View the blog here.](https://blog.cleberg.io)


## Developers
If you want to use any part of this web app, you have full rights to do so. This work is released under the [MIT](https://choosealicense.com/licenses/mit/) license with no conditions or restrictions.

### Installation Setup
To make the app work for your website (without modifying the PHP code), you'll need to do a couple things:
1. Copy the `.php` files and `assets` folder to your website directory (replace the favicons with your own).
2. Create two tables within your database: `article` and `article_body` with the structures shown in the tables below.
3. Create an `.htaccess` file in the root of your directory and include the environment variables code shown below to specify your connection to the database (you'll need to change this code snippet to reflect your actual credentials).

#### Table: `article`
Your `article` table should use the following column types:
1. id:          int(11)
2. title:       varchar(255)
3. author:      varchar(255)
4. description: varchar(255)
5. tag:         varchar(255)
6. created:     datetime
7. modified:    datetime
8. link:        varchar(255)

Your `article` table should look like this when you add data to it:
1. id:          1
2. title:       My First Post
3. author:      Jane Doe
4. description: This is the post where you'll meet me.
5. tag:         Personal
6. created:     2020-02-20 00:00:00
7. modified:    2020-02-27 00:00:00
8. link:        https://example.com/my-first-blog-post.html

#### Table: `article_body`
Your `article_body` table should use the following column types:
1. id:   int(11)
2. body: text

Your `article_body` table should look like this when you add data to it:
1. id:   1
2. body: Markdown for the body of the blog post goes here.

#### File: `.htaccess`
**Note**: An `.htaccess` file only applies to Apache servers. If you use Nginx or another type of server, you will need to set your environment variables appropriately.
```
# Environment Variables
SetEnv DB_SERVER_NAME "server.example.com"
SetEnv USER_NAME "database_admin"
SetEnv PASSWORD "database_password"
SetEnv DB_NAME "database_name"
```
