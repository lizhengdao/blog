<?php

  // SQL Variables
  $servername = getenv('DB_SERVER_NAME');
  $dbname = getenv('DB_NAME');
  $username = $_POST['inputUsername'];
  $password = $_POST['inputPassword'];

  // Blog Post Variables
  $page_title = $_POST['inputPageTitle'];
  $page_link = urlencode($_POST['inputPageLink']);
  $page_description = $_POST['inputPageDescription'];
  $page_category = urlencode($_POST['inputCategory']);
  $page_markdown = htmlspecialchars($_POST['inputMarkdown']);
  $author = 'Christian Cleberg';

  // Connect to Database
  $con = new mysqli($servername, $username, $password, $dbname);
  if ($con->connect_error) {
      die("Connection failed: " . $con->connect_error);
  }

  // Post Article Details to Database
  $sql = "INSERT INTO article (title, author, description, tag, created, modified, link) VALUES ('$page_title', '$author', '$page_description', '$page_category', NOW(), NOW(), '$page_link')";

  if ($con->query($sql) === TRUE) {
    echo "New post details saved successfully.";
  } else {
    echo "Error: " . $sql . "<br>" . $con->error;
  }

  // Post Article Body to Database
  $sqlb = "INSERT INTO article_body (body) VALUES ('$page_markdown')";

  if ($con->query($sqlb) === TRUE) {
    echo "New post markdown saved successfully.";
  } else {
    echo "Error: " . $sqlb . "<br>" . $con->error;
  }

?>
