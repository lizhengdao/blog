<?php

// Gather the search phrase and redirect it to the search results page
if(isset($_GET['searchInput'])) {
    header('Location: https://blog.cleberg.io/search/' . $_GET['searchInput']);
    die();
} else {
    header('Location: https://blog.cleberg.io/404.html');
    die();
}

?>
