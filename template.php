<!doctype html>
<html lang="en">

<head>
    <title><?=$page_title?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="author" content="https://www.cleberg.io">
    <meta name="description" content="<?=$page_description?>">
    <link rel="canonical" href="<?=$page_link?>">
    <?=$page_extras?>
    <link rel="apple-touch-icon" sizes="57x57" href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114"
        href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120"
        href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144"
        href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152"
        href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180"
        href="<?php echo $relative_path; ?>/assets/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"
        href="<?php echo $relative_path; ?>/assets/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32"
        href="<?php echo $relative_path; ?>/assets/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96"
        href="<?php echo $relative_path; ?>/assets/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16"
        href="<?php echo $relative_path; ?>/assets/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo $relative_path; ?>/assets/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?php echo $relative_path; ?>/assets/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?php echo $relative_path; ?>/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo $relative_path; ?>/assets/css/prism.css">
    <style>
        @font-face {
            font-family: 'IBM Plex Sans';
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Regular.eot');
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Regular.woff2') format('woff2'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Regular.woff') format('woff'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Regular.ttf') format('truetype');
        }

        @font-face {
            font-family: 'IBM Plex Sans';
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Bold.eot');
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Bold.woff2') format('woff2'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Bold.woff') format('woff'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexSans-Bold.ttf') format('truetype');
            font-weight: bold;
        }

        @font-face {
            font-family: 'IBM Plex Mono';
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Regular.eot');
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Regular.woff2') format('woff2'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Regular.woff') format('woff'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Regular.ttf') format('truetype');
        }

        @font-face {
            font-family: 'IBM Plex Mono';
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Bold.eot');
            src: url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Bold.woff2') format('woff2'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Bold.woff') format('woff'),
                url('<?php echo $relative_path; ?>/assets/fonts/IBMPlexMono-Bold.ttf') format('truetype');
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet" href="<?php echo $relative_path; ?>/assets/css/app.css">
</head>

<body>
    <div class="banner fixed-top" role="contentinfo">
        <p class="m-0 mr-2">Black Lives Matter.</p>
        <a href="https://developer.ibm.com/callforcode/racial-justice/" target="_blank" rel="noopener">Support the Call for Code®</a>
    </div>
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
        <li class="nav-item d-block d-md-none">
            <a id="sidebarButton" class="nav-link" href="javascript:void(0)" title="Open Navigation Menu"><svg
                    id="icon-side-menu" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32"
                    aria-hidden="true">
                    <path
                        d="M14 4H18V8H14zM4 4H8V8H4zM24 4H28V8H24zM14 14H18V18H14zM4 14H8V18H4zM24 14H28V18H24zM14 24H18V28H14zM4 24H8V28H4zM24 24H28V28H24z">
                    </path>
                </svg></a>
        </li>
        <a class="navbar-brand" href="<?php echo $relative_path; ?>/"><span>Blog</span> | Christian Cleberg</a>

        <ul class="navbar-nav mr-0 ml-auto flex-row">
            <li class="nav-item">
                <a id="search-button" class="nav-link" href="javascript:void(0)" title="Search for articles"><svg
                        id="icon-search" xmlns="http://www.w3.org/2000/svg" description="Open search" width="20"
                        height="20" viewBox="0 0 32 32" aria-hidden="true">
                        <path d="M30,28.59,22.45,21A11,11,0,1,0,21,22.45L28.59,30ZM5,14a9,9,0,1,1,9,9A9,9,0,0,1,5,14Z">
                        </path>
                    </svg></a>
            </li>
        </ul>
        <form id="search-form" class="form-inline h-100 d-none flex-fill" method="get"
            action="<?php echo $relative_path; ?>/search.php">
            <input id="search-input" class="form-control h-100 ml-auto" type="search" placeholder="Search" aria-label="Search"
                name="searchInput">
            <a id="search-close-button" class="nav-link" href="javascript:void(0)" title="Close the search form">
                <svg id="icon-search" xmlns="http://www.w3.org/2000/svg" description="Clear search" width="20"
                    height="20" viewBox="0 0 32 32" aria-hidden="true">
                    <path
                        d="M24 9.4L22.6 8 16 14.6 9.4 8 8 9.4 14.6 16 8 22.6 9.4 24 16 17.4 22.6 24 24 22.6 17.4 16 24 9.4z">
                    </path>
                </svg>
            </a>
        </form>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <div id="sidebar" class="sidebar d-none d-md-block col-md-3 col-lg-2 p-0">
                <div class="sidebar-box p-0">
                    <ul class="categories">
                        <li><a href="<?php echo $relative_path; ?>/category/business">Business</a></li>
                        <li><a href="<?php echo $relative_path; ?>/category/coding">Coding</a></li>
                        <li><a href="<?php echo $relative_path; ?>/category/data-analysis">Data Analysis</a></li>
                        <li><a href="<?php echo $relative_path; ?>/category/hardware">Hardware</a></li>
                        <li><a href="<?php echo $relative_path; ?>/category/personal">Personal</a></li>
                        <li><a href="<?php echo $relative_path; ?>/category/security">Security</a></li>
                        <li><a href="<?php echo $relative_path; ?>/category/software">Software</a></li>
                    </ul>
                    <div>
                        <hr class="sidebar-divider">
                        <ul class="meta">
                            <li>
                                <a href="<?php echo $relative_path; ?>/rss.xml">RSS Feed</a>
                            </li>
                            <li>
                                <a href="https://gitlab.com/christian-cleberg/blog" target="_blank"
                                    rel="noopener">Source Code <svg id="icon-external-link"
                                        xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16"
                                        aria-hidden="true">
                                        <path
                                            d="M13,14H3c-0.6,0-1-0.4-1-1V3c0-0.6,0.4-1,1-1h5v1H3v10h10V8h1v5C14,13.6,13.6,14,13,14z">
                                        </path>
                                        <path d="M10 1L10 2 13.3 2 9 6.3 9.7 7 14 2.7 14 6 15 6 15 1z"></path>
                                    </svg>
                                </a>
                            </li>
                            <li>
                                <a href="https://cleberg.io" target="_blank" rel="noopener">Christian Cleberg &copy;
                                    2020</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="main" class="<?php echo $main_class; ?> col-xs-12 col-md-9 col-lg-10 ml-auto">
                <span class="homepage--dots"></span>
                <?=$content_col?>
            </div>
        </div>
        <button class="btn-to-top" type="button" aria-label="Back to Top">
            <svg id="icon-to-top" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 32 32"
                aria-hidden="true">
                <path d="M16 14L6 24 7.4 25.4 16 16.8 24.6 25.4 26 24zM4 8H28V10H4z"></path>
            </svg>
        </button>

        <!-- JavaScript -->
        <script src="<?php echo $relative_path; ?>/assets/js/jquery-3.5.1.min.js"></script>
        <script src="<?php echo $relative_path; ?>/assets/js/bootstrap.bundle.min.js"></script>
        <script src="<?php echo $relative_path; ?>/assets/js/prism.js"></script>
        <script src="<?php echo $relative_path; ?>/assets/js/app.js"></script>
</body>

</html>
